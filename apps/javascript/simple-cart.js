$(document).ready(function(){
    var Cart = {};
    Cart.items = $('#ahrForm :input');
    Cart.total = 0;
    Cart.cartItems = [];

    Cart.setTotal = function(value, price){
        Cart.total += (value*price);
    };

    Cart.getTotal = function(){
        var $_cart = this;
        var $item;
        var id;
        var value;
        var price;
        Cart.total = 0;
        $.each($_cart.items, function(i,item){
            $item = $(item); 
            id = $item.attr('id');
            value = ($item.attr('value') != '' && isNaN($item.attr('value')) == false) ? parseInt($item.attr('value'), 10) : 0;
            if($item.attr('value') != ''){
                Cart.cartItems.push(item);
            }
            switch(id){
                case 'packageRegistration':
                    price = 45.00;
                    Cart.setTotal(value, price);
                    $item.data('price', price);
                    break;
                case 'lunchAtTheCampusRestaurantAdults':
                    price = 12.50;
                    Cart.setTotal(value, price);
                    $item.data('price', price);
                    break;
                case 'lunchAtTheCampusRestaurantChildren':
                    price = 7.50;
                    Cart.setTotal(value, price);
                    $item.data('price', price);
                    break;
                case 'celebrationBanquet':
                    price = 20.50;
                    Cart.setTotal(value, price);
                    $item.data('price', price);
                    break;
                case 'alumniChildrensBanquet':
                    price = 8.50;
                    Cart.setTotal(value, price);
                    $item.data('price', price);
                    break;
                case 'breakfastForAlumniAdults':
                    price = 10.00;
                    Cart.setTotal(value, price);
                    $item.data('price', price);
                    break;
                case 'breakfastForAlumniChildren':
                    price = 5.00;
                    Cart.setTotal(value, price);
                    $item.data('price', price);
                    break;
                case 'youngAlumniProfessionalHotBreakfast':
                    price = 5.00;
                    Cart.setTotal(value, price);
                    $item.data('price', price);
                    break;
                case 'annualHomecomingLuncheonAdults':
                    price = 12.50;
                    Cart.setTotal(value, price);
                    $item.data('price', price);
                    break;
                case 'annualHomecomingLuncheonChildren':
                    price = 7.50;
                    Cart.setTotal(value, price);
                    $item.data('price', price);
                    break;
                case 'hallofFameGolfTournament:AlumniGame':
                    price = 60.00;
                    Cart.setTotal(value, price);
                    $item.data('price', price);
                    break;
                case 'homecomingBasketballGames':
                    price = 5.00;
                    Cart.setTotal(value, price);
                    $item.data('price', price);
                    break;
                default:
                    price = 0.00;
                    Cart.setTotal(value, price);
                    $item.data('price', price);
                    break;
            }
        });

        if($_cart.total>0){
            $_cart.cartTotal = $_cart.total.toFixed(2);
            $_cart.animate_total_amount(true);
            $_cart.animate_purchase_buttons('show')
        }else{
            $_cart.animate_total_amount(false);
            $_cart.animate_purchase_buttons('hide')
        }
    };

    Cart.set_total = function(){
        $_cart = this;
        $.each($_cart.items, function(){
            $_item = $(this);
            if($_item.attr('type') == 'text'){
                $_item.blur(function(){
                    Cart.getTotal();
                });

                $_item.click(function(){
                    if($(this).hasClass('userText')){
                        return false;
                    }else{
                        $(this).attr('value', '');
                        Cart.getTotal();
                    }
                });
            }
        });
    };
	
    Cart.PurchaseButton = function(){
        $( "#purchase" ).button().hide();
        $( "#purchase" ).click(function() { 
            return false;
        });
    };

    Cart.PurchaseButton();

    Cart.PurchaseItems = function(element){
        var $_cart = this;
        $(element).click(function(e){
            $_cart.scroll_top();
            var $item;
            var price;
            var num = 0;
            var text = '<p class="bold blueText">Confirm Order - Total Amount: $' + Cart.cartTotal + '</p>';
            text += '<table>';
            text += '<tbody>'
            text += '<tr><th>Item</th><th>Name of Event</th><th>Qty / Value</th><th>Price</th></tr>'
            $.each($('#ahrForm :input'), function(i, item){
                $item = $(this);
                if($item.attr('id') != undefined && $item.attr('type') == 'text' && $item.attr('value') != '' && $item.hasClass('userText') == false){
                    num = ++num;
                    text += '<tr>' +
                    '<td>' + num + '</td><td>' + $item.attr('title') + '</td><td>' + $item.attr('value') + '</td><td>' + $item.data('price').toFixed(2) + '</td>' +
                    '</tr>';
                }else if($item.hasClass('userText') && $item.attr('value') != ''){
                    num = ++num;
                    text += '<tr>' +
                    '<td>' + num + '</td><td>' + $item.attr('title') + '</td><td>' + $item.attr('value') + '</td><td>&nbsp; </td>' +
                    '</tr>';

                }
            });
            text += '</tbody>';
            text += '</table>';
            var cartItemsButton = '<input id="cartItemsButton" type="button" value="Continue Shopping" />';
            if($('#cartItems').length == 0){
                $('<div id="cartItems" class="ui-widget-content ui-corner-all">' + text + '<br />' + cartItemsButton + '</div>')
                .appendTo($('body :eq(1)'))
                .hide()
                .slideDown('slow');
                
                $_cart.checkout_button($('#cartItems'));
                $_cart.load_payment_form();
            }else{
                $('#cartItems').html(text + '<br />' +  cartItemsButton).slideDown('slow');
                $_cart.checkout_button($('#cartItems'));
                $_cart.load_payment_form();
            }

            $('#cartItemsButton').button().click(function(){
                $('#cartItems').slideUp('slow');
            })

            e.preventDefault();
        });
    };

    Cart.scroll_top = function(){
        //$('html, body').animate({'scrollTop': $('body').scrollTop()});
        $('html, body').animate({
            'scrollTop': 0
        }, 'slow');
    };

    Cart.animate_purchase_buttons = function(show){
        $button = $('input.purchase_button');

        if(show=='hide'){
            $button.hide('fast');
        }else if(show=='show'){
            $button.button().show('slow');
        }
    };

    Cart.animate_total_amount = function(total){
        var $_cart = this;
        var html = '';
        var $cart_total = $('span.cart_total');
        if(total == true){
            html = '<span class="blueText bold">Cart total: $' + $_cart.total.toFixed(2) + '</span>'
            $cart_total.html(html);
        }else{
            html = '<span class="blueText bold">Your Cart is empty:</span>';
            $cart_total.html(html);
        }
    };

    Cart.checkout_button = function(parent_container){
        if($('#checkoutButton').length < 1 && $(parent_container).length > 0){
            var $_cart = this;
            var $_checkout_button = $('<input id="checkoutButton" type="button" value="Check Out" />')
            .button()
            .click(function(e){
                $_cart.required_fields();
            })
            .appendTo($(parent_container));
        }
    };

    Cart.required_fields = function(){
        $_cart = this;
        var $required_fields = $('input.requiredField');
        var valid = true;

        $.each($required_fields, function(i){
            if ($(this).attr('value') == ''){
                $(this).addClass('error-required');

                valid = false;
            }else{
                $(this).removeClass('error-required');
            }
        });
        if($('input.error-required').filter(':first').length > 0){
            $('input.error-required').filter(':first').focus();
        }else{
            $_cart.send_transaction();
        }
    };

    Cart.loading = function (container){
        container = container != '' ? container : 'body'
        $(container).css('overflow-y', 'hidden'); 
        $('<div id="overlay"></div>') 
        .css('top', $(document).scrollTop()) 
        .css('opacity', '0') 
        .animate({
            'opacity': '0.5'
        }, 'fast') 
        .appendTo(container); 
    };

    Cart.remove_loading = function (){
        $('#overlay').remove();
    }

    Cart.spinner = function(container) {
        $('<div></div>') 
        .attr('id', 'spinner') 
        .hide() 
        .appendTo(container) 
        .fadeTo('slow', 0.6);
    };

    Cart.remove_spinner = function (){
        $('#spinner').fadeOut('slow', function() {
            $(this).remove();
        });
    };

    Cart.send_transaction = function() {
        $_cart = this;
        var data = {
            transaction_amount: $_cart.cartTotal, 
            cc_name: $('#name').attr('value'), 
            cc_address: $('input[name="cc_address"]').attr('value'),
            cc_zip: $('input[name="cc_zip"]').attr('value'),
            card_number_encode: $('input[name="card_number_encode"]').attr('value'),
            cc_exp_date_month: $('select[name="cc_exp_date.month"] option:selected').attr('value'),
            cc_exp_date_year: $('select[name="cc_exp_date.year"] option:selected').attr('value'),
            cvv2_encode: $('input[name="cvv2_encode"]').attr('value'),
            packageRegistration: $('#packageRegistration').attr('value'),// Main form elements
            lunchAtTheCampusRestaurantAdults: $('#lunchAtTheCampusRestaurantAdults').attr('value'),
            lunchAtTheCampusRestaurantChildren: $('#lunchAtTheCampusRestaurantChildren').attr('value'),
            celebrationBanquet: $('#celebrationBanquet').attr('value'),
            alumniChildrensBanquet: $('#alumniChildrensBanquet').attr('value'),
            musicConcertAndAwardPresentation: $('#musicConcertAndAwardPresentation').attr('value'),
            breakfastForAlumniAdults: $('#breakfastForAlumniAdults').attr('value'),
            breakfastForAlumniChildren: $('#breakfastForAlumniChildren').attr('value'),
            youngAlumniProfessionalHotBreakfast: $('#youngAlumniProfessionalHotBreakfast').attr('value'),
            annualHomecomingLuncheonAdults: $('#annualHomecomingLuncheonAdults').attr('value'),
            annualHomecomingLuncheonChildren: $('#annualHomecomingLuncheonChildren').attr('value'),
            classOfEarly90sTourToLakesideVillage: $('#classOfEarly90sTourToLakesideVillage').attr('value'),
            studentLeadersReunion: $('#studentLeadersReunion').attr('value'),
            MBA: $('#MBA').attr('value'),
            MAML: $('#MAML').attr('value'),
            MED: $('#MED').attr('value'),
            MA: $('#MA').attr('value'),
            MS: $('#MS').attr('value'),
            hallofFameGolfTournamentAlumniGame: $('#hallofFameGolfTournamentAlumniGame').attr('value'),
            homecomingBasketballGames: $('#homecomingBasketballGames').attr('value'),
            mensSoccer: $('#mensSoccer').attr('value'),
            mensBasketball: $('#mensBasketball').attr('value'),
            womensVolleyball: $('#womensVolleyball').attr('value'),
            womensTennis: $('#womensTennis').attr('value'),
            womensBasketball: $('#womensBasketball').attr('value'),
            womensSoccer: $('#womensSoccer').attr('value'),
            cheerleading: $('#cheerleading').attr('value'),
            classOf1962: $('#classOf1962').attr('value'),
            classOf1972: $('#classOf1972').attr('value'),
            classOf1982Adults: $('#classOf1982Adults').attr('value'),
            classOf1982Children: $('#classOf1982Children').attr('value'),
            classOf1987Adults: $('#classOf1987Adults').attr('value'),
            classOf1987Children: $('#classOf1987Children').attr('value'),
            classOf2002Adults: $('#classOf2002Adults').attr('value'),
            classOf2002Children: $('#classOf2002Children').attr('value'),
            tailGatePartyAndDinnerAdults: $('#tailGatePartyAndDinnerAdults').attr('value'),
            tailGatePartyAndDinnerChildren: $('#tailGatePartyAndDinnerChildren').attr('value'),
            name: $('#name').attr('value'),
            classYear: $('#classYear').attr('value'),
            address: $('#address').attr('value'),
            city: $('#city').attr('value'),
            state: $('#state').attr('value'),
            zip: $('#zip').attr('value'),
            yourName: $('#yourName').attr('value'),
            guestOne: $('#guestOne').attr('value'),
            guestTwo: $('#guestTwo').attr('value'),
            guestThree: $('#guestThree').attr('value')
        }

        $.ajax({ 
            type: "POST",
            url: 'http://' + document.location.host + '/seu/apps/index.php/tridentcart/',
            data: data, 
            beforeSend: function() { 
                $_cart.loading('#cartItems')
            }, 
            error: function() { 
                $('<p id="message">Failed, please try again.</p>').insertBefore('#cartItemsButton');
            }, 
            success: function(data) { 
                if($('#checkoutResults').length > 0){
                    $('#checkoutResults').remove();
                }

                $(data).insertBefore('#cartItemsButton'); 
            }, 
            complete: function() { 
                $_cart.remove_loading(); 
            } 
        }); 
    };

    Cart.load_payment_form = function() {
        $_cart = this;
        $('<p id="payment_form"></p>').insertBefore('#cartItemsButton');
        $('#payment_form').html('loading...').load('http://simplyad.com/seu/apps/index.php/ahr/tridentpayform/', {
            'total': $_cart.cartTotal
        });
    }

    Cart.set_total();
    Cart.getTotal();
    Cart.PurchaseItems('.purchaseBottom');
    Cart.PurchaseItems('.purchase');
});
