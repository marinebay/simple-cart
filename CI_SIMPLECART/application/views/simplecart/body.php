        <div id="container">
            <div id="header">
                <h1>Welcome Home</h1>
                <h2><i>Alumni Homecoming Reunion</i> | FEBRUARY 3-4, 2012</h2>
            </div>
            <div id="content">
                <form id="ahrForm" action="">
                    <span class="cart_total cartTotal paddSpanRight"></span><input class="purchase_button purchase" type="submit" value="Purchase Tickets" />
                    <ul class="formUl" style="padding:0;">
                        <li class="clearBoth noListStyle">
                            <label for="packageRegistration"><span class="packageRegistration">PACKAGE REGISTRATION</span> 45.00</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="packageRegistration"  title="PACKAGE REGISTRATION"/><span class="liMessage">Includes the Celebration Banquet, Saturday Homecoming Luncheon, Tailgate Party Dinner, Lunch Friday afternoon OR Breakfast on Saturday morning, and a ticket to the Homecoming game.</span>
                        </li>
                    </ul>
                    <h3>SATURDAY, FEBRUARY 3</h3>
                    <ul class="formUl" style="padding:0;">
                        <li class="clearBoth">
                            <label for="campusTour" class="liPadOne"><strong>Campus Tour</strong> - FREE</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="campusTour" title="Campus Tour" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="lunchAtTheCampusRestaurantAdults"><strong>Lunch at the campus restaurant (Buffet)</strong> [Adults] 12.50</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="lunchAtTheCampusRestaurantAdults" title="Lunch at the campus restaurant (Buffet) [Adults]" />
                            <label class="liPadTwo" for="lunchAtTheCampusRestaurantChildren">[Children] 7.50</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="lunchAtTheCampusRestaurantChildren" title="Lunch at the campus restaurant (Buffet) [Children]" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="celebrationBanquet"><strong>Celebration Banquet</strong> 20.00</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="celebrationBanquet" title="Celebration Banquet" /><br /><i>Limited seating. Please reply by January 15, 2012</i>
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="alumniChildrensBanquet"><strong>Alumni Children's Banquet</strong> 8.50</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="alumniChildrensBanquet" title="Alumni Children's Banquet" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="musicConcertAndAwardPresentation"><strong>Music Concert and Award Presentation</strong> - FREE</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="musicConcertAndAwardPresentation" title="Music Concert and Award Presentation" />
                        </li>
                    </ul>
                    <h3>SATURDAY, FEBRUARY 4</h3>
                    <ul class="formUl" style="padding:0;">
                        <li class="clearBoth">
                            <label class="liPadOne" for="breakfastForAlumniAdults"><strong>Breakfast for Alumni</strong> [Adults] 10.00</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="breakfastForAlumniAdults" title="Breakfast for Alumni [Adults]" />
                            <label class="liPadTwo" for="breakfastForAlumniChildren">[Children] 5.00</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="breakfastForAlumniChildren" title="Breakfast for Alumni [Children]" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="youngAlumniProfessionalHotBreakfast"><strong>Young Alumni Professional Hot Breakfast</strong> 10.00</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="youngAlumniProfessionalHotBreakfast" title="Young Alumni Professional Hot Breakfast" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="annualHomecomingLuncheonAdults"><strong>Annual Homecoming Luncheon</strong> [Adults] 12.50</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="annualHomecomingLuncheonAdults" title="Annual Homecoming Luncheon [Adults]" />
                            <label class="liPadTwo" for="annualHomecomingLuncheonChildren">[Children] 7.50</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="annualHomecomingLuncheonChildren" title="Annual Homecoming Luncheon [Children]" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="classOfEarly90sTourToLakesideVillage"><strong>Class of Early 90's tour to Lakeside Village</strong> - FREE</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="classOfEarly90sTourToLakesideVillage" title="Class of Early 90's tour to Lakeside Village" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="studentLeadersReunion"><strong>Student Leaders Reunion</strong> - FREE</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="studentLeadersReunion" title="Student Leaders Reunion" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="MBA"><strong>Graduate Class Reunions</strong> - FREE <br />
                            MBA </label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="MBA" title="MBA" />
                            <label for="MAML">MAML </label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="MAML" title="Graduate Class Reunions MAML"/>
                            <label for="MED">MED </label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="MED" title="Graduate Class Reunions MED" />
                            <label for="MED">MA </label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="MA" title="Graduate Class Reunions MA" />
                            <label for="MED">MS </label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="MS" title="Graduate Class Reunions MS" />
                        </li>
                        <li class="clearBoth noListStyle">
                            <label for="hallofFameGolfTournamentAlumniGame"><strong>Hall of Fame Golf Tournament: Alumni Game</strong> 60.00</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="hallofFameGolfTournamentAlumniGame" title="Hall of Fame Golf Tournament: Alumni Game" />
                        </li>
                        <li class="clearBoth noListStyle">
                            <label for="homecomingBasketballGames"><strong>Homecoming Basketball Games</strong> 5.00</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="homecomingBasketballGames" title="Homecoming Basketball Games" />
                        </li>
                        <li class="clearBoth noListStyle noPadLeft blueText">
                            <strong>ALUMNI GAMES</strong> - FREE
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="mensSoccer"><strong>Men's Soccer</strong></label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="mensSoccer" title="Men's Soccer" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="mensBasketball"><strong>Men's Basketball</strong></label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="mensBasketball" title="Men's Basketball" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="womensVolleyball"><strong>Wemon's Volleyball</strong></label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="womensVolleyball" title="Wemon's Volleyball" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="womensTennis"><strong>Wemon's Tennis</strong></label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="womensTennis" title="Wemon's Tennis" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="womensBasketball"><strong>Wemon's Basketball</strong></label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="womensBasketball" title="Wemon's Basketball" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="womensSoccer"><strong>Wemon's Soccer</strong></label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="womensSoccer" title="Wemon's Soccer" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="cheerleading"><strong>Cheerleading</strong></label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="cheerleading" title="Cheerleading" />
                        </li>
                        <li class="clearBoth noListStyle noPadLeft blueText">
                            <strong>CLASS REUNION ACTIVITIES</strong> - FREE
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="classOf1962"><strong>Class of 1962</strong> [Adults]</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="classOf1962" title="Class of 1962 [Adults]" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="classOf1972"><strong>Class of 1972</strong> [Adults]</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="classOf1972" title="Class of 1972 [Adults]" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="classOf1982Adults"><strong>Class of 1982</strong> [Adults]</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="classOf1982Adults" title="Class of 1982 [Adults]" />
                            <label class="liPadTwo" for="classOf1982Children">[Children]</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="classOf1982Children" title="Class of 1982 [Children]" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="classOf1987Adults"><strong>Class of 1987</strong> [Adults]</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="classOf1987Adults" title="Class of 1987 [Adults]" />
                            <label class="liPadTwo" for="classOf1987Children">[Children]</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="classOf1987Children" title="Class of 1987 [Children]" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="classOfEarly90sAdults"><strong>Class of Early 90's (Special Emphasis on 1992)</strong> [Adults]</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="classOfEarly90sAdults" title="Class of Early 90's (Special Emphasis on 1992) [Adults]" />
                            <label class="liPadTwo" for="classOfEarly90sChildren">[Children]</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="classOfEarly90sChildren" title="Class of Early 90's (Special Emphasis on 1992) [Children]" />
                        </li>
                        <li class="clearBoth">
                            <label class="liPadOne" for="classOf2002Adults"><strong>Class of 2002</strong> [Adults]</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="classOf2002Adults" title="Class of 2002 [Adults]" />
                            <label class="liPadTwo" for="classOf2002Children">[Children]</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="classOf2002Children" title="Class of 2002 [Children]" />
                        </li>
                        <li class="clearBoth noListStyle blueText">
                            <label class="liPadOne" for="tailGatePartyAndDinnerAdults"><strong>TAIL GATE PARTY AND DINNER</strong> [Adults]</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="tailGatePartyAndDinnerAdults" title="TAIL GATE PARTY AND DINNE [Adults]" />
                            <label class="noListStylelabelPadTwo" for="tailGatePartyAndDinnerChildren">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Children]</label><input maxlength="3" class="count ui-widget-content ui-corner-all" type="text" id="tailGatePartyAndDinnerChildren" title="TAIL GATE PARTY AND DINNER [Children]" />
                        </li>
                    </ul>
                    <p class="required-fields crudentialsPadding">Required fields<span class="red-text">&nbsp;*</span></p>
                    <ul class="crudentialsPadding">
                        <li class="noListStyle displayInline"><label class="bold" for="name"><span class="red-text">*</span>Name</label></li>
                        <li class="noListStyle displayInline"><input class="userText ui-widget-content ui-corner-all requiredField" type="text" id="name" title="Name" /></li>
                    </ul>
                    <ul class="crudentialsPadding">
                        <li class="noListStyle displayInline"><label class="bold" for="classYear"><span class="red-text">*</span>Class Year</label></li>
                        <li class="noListStyle displayInline"><input class="userText ui-widget-content ui-corner-all requiredField" type="text" id="classYear" title="Class Year" /></li>
                    </ul>
                    <ul class="crudentialsPadding">
                        <li class="noListStyle displayInline"><label class="bold" for="address"><span class="red-text">*</span>Address</label></li>
                        <li class="noListStyle displayInline"><input class="userText ui-widget-content ui-corner-all requiredField" type="text" id="address" title="Address" /></li>
                    </ul>
                    <ul class="crudentialsPadding">
                        <li class="noListStyle displayInline"><label class="bold" for="city"><span class="red-text">*</span>City</label></li>
                        <li class="noListStyle displayInline"><input class="userText ui-widget-content ui-corner-all requiredField" type="text" id="city" title="City" /></li>
                    </ul>
                    <ul class="crudentialsPadding">
                        <li class="noListStyle displayInline"><label class="bold" for="state"><span class="red-text">*</span>State</label></li>
                        <li class="noListStyle displayInline"><input class="userText count ui-widget-content ui-corner-all requiredField" type="text" id="state" title="State" /></li>
                    </ul>
                    <ul class="crudentialsPadding">
                        <li class="noListStyle displayInline"><label class="bold" for="zip"><span class="red-text">*</span>Zip</label></li>
                        <li class="noListStyle displayInline"><input class="userText ui-widget-content ui-corner-all requiredField" type="text" id="zip" title="Zip" /></li>
                    </ul>
                    <span class="clear"></span>
                    <p class="crudentialsPadding">
                        Please print names as you wish to appear on name badges;<br />
                        include class years if applicable:
                    </p>
                    <ul class="crudentialsPadding">
                        <li class="noListStyle displayInline"><label class="bold" for="yourName">Your Name</label></li>
                        <li class="noListStyle displayInline"><input class="userText ui-widget-content ui-corner-all" type="text" id="yourName" title="Name badge: Your Name" /></li>
                    </ul>
                    <ul class="crudentialsPadding">
                        <li class="noListStyle displayInline"><label class="bold" for="guestOne">Guest One</label></li>
                        <li class="noListStyle displayInline"><input class="userText ui-widget-content ui-corner-all" type="text" id="guestOne" title="Name badge: Guest one"/></li>
                    </ul>
                    <ul class="crudentialsPadding">
                        <li class="noListStyle displayInline"><label class="bold" for="guestTwo">Guest Two</label></li>
                        <li class="noListStyle displayInline"><input class="userText ui-widget-content ui-corner-all" type="text" id="guestTwo" title="Name badge: Guest two" /></li>
                    </ul>
                    <ul class="crudentialsPadding">
                        <li class="noListStyle displayInline"><label class="bold" for="guestThree">Guest Three</label></li>
                        <li class="noListStyle displayInline"><input class="userText ui-widget-content ui-corner-all" type="text" id="guestThree" title="Name badge: Guest three" /></li>
                    </ul>
                    <span class="clear spacer"></span>
                    <span class="cart_total cartTotalBottom paddSpanRight"></span><input class="purchase_button purchaseBottom" type="submit" value="Purchase Tickets" />
                    <span class="clear spacer"></span>
                </form>
            </div>
        </div>
