    <div id="paymentInfoHeader">
      <div class="formSectionHeader bold blueText">
        Card Information
      </div>
    </div>

    <div id="transactionInformation">
      <table width="100%">
        <tbody>
          <tr>
            <td class="formLabelText">Currency code &nbsp;</td>

            <td class="formDataText">USD &nbsp;</td>
          </tr>

          <tr>
            <td class="formLabelText">Transaction Amount&nbsp;</td>

            <td class="bold blue formDataText"><?=isset($_REQUEST['total']) ? $_REQUEST['total'] : 'NA';?>&nbsp;</td>
          </tr>

          <tr>
            <td class="formLabelText">Address <i>as it appears on card:</i>&nbsp;</td>

            <td class="formDataText"><input type="text" autocomplete="off" class=
            "requiredField formDataText" value="" name="cc_address" maxlength=
            "75" size="24" /></td>
          </tr>

          <tr>
            <td class="formLabelText">Zip Code <i>as it appears on card:</i>&nbsp;</td>

            <td class="formDataText"><input type="text" autocomplete="off" class=
            "requiredField formDataText" value="" name="cc_zip" maxlength=
            "75" size="24" /></td>
          </tr>

          <!---
          <tr>
            <td class="formLabelText"></td>

            <td valign="bottom" class="formDataText"><img src=
            "/images/card_logo_visa.JPG" /> <img src=
            "/images/card_logo_mastercard.JPG" /><img src=
            "/images/card_logo_amex.JPG" /><img src="/images/card_logo_disc.JPG" /></td>
          </tr>
          --->

          <tr>
            <td class="formLabelText">Account Number&nbsp;</td>

            <td class="formDataText"><input type="text" autocomplete="off" class=
            "requiredField formDataText" value="" name="card_number_encode" maxlength=
            "16" size="24" /></td>
          </tr>

          <tr>
            <td class="formLabelText">Expiration Date &nbsp;</td>

            <td class="formDataText"><select name="cc_exp_date.month" class=
            "formDataText">
              <option selected="selected" value="0">
                01 - January
              </option>

              <option value="1">
                02 - February
              </option>

              <option value="2">
                03 - March
              </option>

              <option value="3">
                04 - April
              </option>

              <option value="4">
                05 - May
              </option>

              <option value="5">
                06 - June
              </option>

              <option value="6">
                07 - July
              </option>

              <option value="7">
                08 - August
              </option>

              <option value="8">
                09 - September
              </option>

              <option value="9">
                10 - October
              </option>

              <option value="10">
                11 - November
              </option>

              <option selected="selected" value="11">
                12 - December
              </option>
            </select> <select name="cc_exp_date.year" class="formDataText">
              <option value="2011">
                2011
              </option>

              <option value="2012">
                2012
              </option>

              <option value="2013">
                2013
              </option>

              <option value="2014">
                2014
              </option>

              <option value="2015">
                2015
              </option>

              <option selected="selected" value="2016">
                2016
              </option>

              <option value="2017">
                2017
              </option>

              <option value="2018">
                2018
              </option>

              <option value="2019">
                2019
              </option>

              <option value="2020">
                2020
              </option>

              <option value="2021">
                2021
              </option>
            </select></td>
          </tr>

          <tr>
            <td class="formLabelText">Security Code&nbsp;</td>

            <td class="formDataText"><input type="text" class="requiredField formDataText" value=""
            name="cvv2_encode" maxlength="4" size="5" /><font size="1">&nbsp;</font></td>
          </tr>
        </tbody>
      </table>
    </div>
