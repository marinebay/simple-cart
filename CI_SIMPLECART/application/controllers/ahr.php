<?php
//class BayAreaNew extends CI_Controller {
class Ahr extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $this->load->view('simplecart/header');
        $this->load->view('simplecart/body');
        $this->load->view('simplecart/footer');
	}

    public function tridentpayform(){
        $this->load->view('simplecart/tridentpayform');
	}
}
