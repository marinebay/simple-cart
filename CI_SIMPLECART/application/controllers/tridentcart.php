<?php
class TridentCart extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library('TridentGateway');
        $this->load->library('CreateCsv');
    }

    public function index(){
        $data = array();
        $data['name'] = $this->input->post('cc_name', TRUE);
        $data['address'] = $this->input->post('cc_address', TRUE);
        $data['zip'] = $this->input->post('cc_zip', TRUE);
        $data['cvv2_encode'] = $this->input->post('cvv2_encode', TRUE);
        $data['card_number_encode'] = $this->input->post('card_number_encode', TRUE);
        $data['cc_exp_date_month'] = $this->input->post('cc_exp_date_month', TRUE);
        $data['cc_exp_date_year'] = $this->input->post('cc_exp_date_year', TRUE);
        $data['transaction_amount'] = $this->input->post('transaction_amount', TRUE);

        $results = $this->tridentgateway->make_transaction($data);

        if(preg_match('/Approval/', $results['confirmation'])){
            $this->writetofile($results);
        }

        $this->load->view('simplecart/tridentconfirmation', $results);
    }

    public function tridentpayform($data){
        $result['total'] = $this->input->post('total', TRUE);
        $this->load->view('simplecart/tridentpayform', $result);
    }

    public function ahrouttocsv(){
        $this->output->set_header("Pragma: public");
        $this->output->set_header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        $this->output->set_header("Cache-Control: private",false);
        $this->output->set_header("Content-Type: application/octet-stream");
        $csv_file_path =  realpath('../views/simplecart/xml/'). DIRECTORY_SEPARATOR . 'ahrlist.csv';
        $csv_file = "Content-Disposition: attachment; filename=\"ahrlist.csv\";";
        header($csv_file);
        readfile(dirname(__FILE__) . '/../views/simplecart/xml/ahrlist.csv');
        $this->output->set_header("Content-Transfer-Encoding: binary");
        //$this->load->view('simplecart/xml/ahrlist.csv');
    }

    public function writetofile($auth=array()){
        $data = array();
        $data['auth_code'] = $auth['auth_code'];
        $data['date'] = date('j-n-Y');
        $data['invoice_number'] = $auth['invoice_number'];
        $data['packageRegistration'] = $this->input->post('packageRegistration', TRUE);
        $data['campusTour'] = $this->input->post('campusTour', TRUE);
        $data['lunchAtTheCampusRestaurantAdults'] = $this->input->post('lunchAtTheCampusRestaurantAdults', TRUE);
        $data['lunchAtTheCampusRestaurantChildren'] = $this->input->post('lunchAtTheCampusRestaurantChildren', TRUE);
        $data['celebrationBanquet'] = $this->input->post('celebrationBanquet', TRUE);
        $data['alumniChildrensBanquet'] = $this->input->post('alumniChildrensBanquet', TRUE);
        $data['musicConcertAndAwardPresentation'] = $this->input->post('musicConcertAndAwardPresentation', TRUE);
        $data['breakfastForAlumniAdults'] = $this->input->post('breakfastForAlumniAdults', TRUE);
        $data['breakfastForAlumniChildren'] = $this->input->post('breakfastForAlumniChildren', TRUE);
        $data['youngAlumniProfessionalHotBreakfast'] = $this->input->post('youngAlumniProfessionalHotBreakfast', TRUE);
        $data['annualHomecomingLuncheonAdults'] = $this->input->post('annualHomecomingLuncheonAdults', TRUE);
        $data['annualHomecomingLuncheonChildren'] = $this->input->post('annualHomecomingLuncheonChildren', TRUE);
        $data['classOfEarly90sTourToLakesideVillage'] = $this->input->post('classOfEarly90sTourToLakesideVillage', TRUE);
        $data['studentLeadersReunion'] = $this->input->post('studentLeadersReunion', TRUE);
        $data['MBA'] = $this->input->post('MBA', TRUE);
        $data['MAML'] = $this->input->post('MAML', TRUE);
        $data['MED'] = $this->input->post('MED', TRUE);
        $data['MA'] = $this->input->post('MA', TRUE);
        $data['MS'] = $this->input->post('MS', TRUE);
        $data['hallofFameGolfTournamentAlumniGame'] = $this->input->post('hallofFameGolfTournamentAlumniGame', TRUE);
        $data['homecomingBasketballGames'] = $this->input->post('homecomingBasketballGames', TRUE);
        $data['mensSoccer'] = $this->input->post('mensSoccer', TRUE);
        $data['mensBasketball'] = $this->input->post('mensBasketball', TRUE);
        $data['womensVolleyball'] = $this->input->post('womensVolleyball', TRUE);
        $data['womensTennis'] = $this->input->post('womensTennis', TRUE);
        $data['womensBasketball'] = $this->input->post('womensBasketball', TRUE);
        $data['womensSoccer'] = $this->input->post('womensSoccer', TRUE);
        $data['cheerleading'] = $this->input->post('cheerleading', TRUE);
        $data['classOf1962'] = $this->input->post('classOf1962', TRUE);
        $data['classOf1972'] = $this->input->post('classOf1972', TRUE);
        $data['classOf1982Adults'] = $this->input->post('classOf1982Adults', TRUE);
        $data['classOf1982Children'] = $this->input->post('classOf1982Children', TRUE);
        $data['classOf1987Adults'] = $this->input->post('classOf1987Adults', TRUE);
        $data['classOf1987Children'] = $this->input->post('classOf1987Children', TRUE);
        $data['classOf1987Adults'] = $this->input->post('classOf1987Adults', TRUE);
        $data['classOf1987Children'] = $this->input->post('classOf1987Children', TRUE);
        $data['classOfEarly90sAdults'] = $this->input->post('classOfEarly90sAdults', TRUE);
        $data['classOfEarly90sChildren'] = $this->input->post('classOfEarly90sChildren', TRUE);
        $data['classOf2002Adults'] = $this->input->post('classOf2002Adults', TRUE);
        $data['classOf2002Children'] = $this->input->post('classOf2002Children', TRUE);
        $data['tailGatePartyAndDinnerAdults'] = $this->input->post('tailGatePartyAndDinnerAdults', TRUE);
        $data['tailGatePartyAndDinnerChildren'] = $this->input->post('tailGatePartyAndDinnerChildren', TRUE);
        $data['name'] = $this->input->post('name', TRUE);
        $data['classYear'] = $this->input->post('classYear', TRUE);
        $data['address'] = $this->input->post('address', TRUE);
        $data['city'] = $this->input->post('city', TRUE);
        $data['state'] = $this->input->post('state', TRUE);
        $data['zip'] = $this->input->post('zip', TRUE);
        $data['yourName'] = $this->input->post('yourName', TRUE);
        $data['guestOne'] = $this->input->post('guestOne', TRUE);
        $data['guestTwo'] = $this->input->post('guestTwo', TRUE);
        $data['guestThree'] = $this->input->post('guestThree', TRUE);

        $this->createcsv->put($data, dirname(__FILE__) . '/../views/simplecart/xml/ahrlist.csv');
    }
}
