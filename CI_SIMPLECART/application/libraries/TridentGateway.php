<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('PROFILEID', '94100010433200000222');
define('PROFILEKEY', 'PumZzdxWoclVMlWsFKurnKTYtDQDlzBt');
define('HOSTURI', 'https://cert.merchante-solutions.com/mes-api/tridentApi');

class TridentGateway {

    public function make_transaction($data=array()){
        require_once 'trident_gateway.php';
        $invoice_number = $_SERVER['REQUEST_TIME'];
        $tran = new TpgSale( PROFILEID, PROFILEKEY );
        $tran->setAvsRequest( $data['address'], $data['zip'] );
        $tran->setRequestField( 'cvv2', $data['cvv2_encode'] );
        $tran->setRequestField('invoice_number', $invoice_number);
        $tran->setTransactionData( $data['card_number_encode'],
            ($data['cc_exp_date_month']+1).substr($data['cc_exp_date_year'], -2), $data['transaction_amount'] );
        $tran->setHost( HOSTURI );
        $tran->execute();
        $results = array();

        if( $tran->isApproved() ){
            $results['auth_code'] = $tran->ResponseFields['auth_code'];
            $results['invoice_number'] = $invoice_number;
            $results['confirmation'] = '<div id="checkoutResults" class="ui-state-highlight ui-corner-all message">' .
                '<p>Thank you for your purchase.' .
                '<br />Invoice : ' . $invoice_number .
                '<br />Approval: ' . $results['auth_code'] .
                '<br />Your transaction was successful' .
                '</p></div>';

        }
        else{
          $error_message = !empty($tran->ResponseFields['auth_response_text']) ? '<br />Error message: ' . $tran->ResponseFields['auth_response_text'] : '';
          $results['confirmation'] = '<div id="checkoutResults" class="ui-state-error ui-corner-all message">' .
              '<p>We were unable to process your transaction.' .
              '<br />The transaction was declined.' .
              $error_message . '</p></div>';
        }

        return $results;
    }
}

