<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class CreateCsv{

    public function put($data=array(), $file){ 
    	$csv_data = array_values($data);
        $csv_out = implode(',', $csv_data);
        
        $fh = fopen($file, 'a');
        fwrite($fh, $csv_out . "\n");
        fclose($fh);
	}
}
